//
//  UIViewCOn.swift
//  LifePlusDemo
//
//  Created by Touhid on 19/3/21.
//

import UIKit
import Nuke

extension UIViewController {
    
    func transitionVc(vc: UIViewController, type: CATransitionSubtype) {
        let customVcTransition = vc
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = type
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        view.window?.layer.add(transition, forKey: kCATransition)
        present(customVcTransition, animated: false, completion: nil)
    } // transitionVc
    
    func transitionVcDismiss(vc: UIViewController, type: CATransitionSubtype, completion:@escaping (() -> Void)) {
        
        let transition: CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.reveal
        transition.subtype = type
        vc.view.window!.layer.add(transition, forKey: nil)
       // vc.dismiss(animated: false, completion: nil)
        
        vc.dismiss(animated: false) {
            completion()
        }
        
    } // transitionVcDismiss
    
} // UIViewController


extension UIImageView {
    
    func loadFromUrl(path: String) {
        
        if let mUrl = URL(string: path) {
            Nuke.loadImage(with: mUrl, into: self)
        }
        
    }
    
} // UIImageView


extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String { html2AttributedString?.string ?? "" }
} // Data

extension StringProtocol {
    var html2AttributedString: NSAttributedString? {
        Data(utf8).html2AttributedString
    }
    var html2String: String {
        html2AttributedString?.string ?? ""
    }
} // StringProtocol
