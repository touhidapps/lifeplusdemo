//
//  MyUser+CoreDataProperties.swift
//  LifePlusDemo
//
//  Created by Touhid on 19/3/21.
//

import Foundation
import CoreData


extension MyUser {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MyUser> {
        return NSFetchRequest<MyUser>(entityName: "MyUser")
    }

    @NSManaged public var id       : Int64
    @NSManaged public var name     : String?
    @NSManaged public var phone    : String?
    @NSManaged public var userName : String?
    @NSManaged public var password : String?

}
