//
//  ShowCell.swift
//  LifePlusDemo
//
//  Created by Touhid on 19/3/21.
//

import UIKit

class ShowCell: UICollectionViewCell {
    
    @IBOutlet var imageViewThumb   : UIImageView!
    @IBOutlet var labelTitle       : UILabel!
    @IBOutlet var labelDescription : UILabel!
    
}
