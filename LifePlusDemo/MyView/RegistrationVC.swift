//
//  SplashVC.swift
//  DemoProject
//
//  Created by Touhid on 3/17/21.
//  Copyright © 2021 Touhid. All rights reserved.
//

import UIKit
import CoreData

class RegistrationVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var tfName    : UITextField!
    @IBOutlet var tfUserName: UITextField!
    @IBOutlet var tfPassword: UITextField!
    @IBOutlet var tfPhone   : UITextField!
    
    @IBAction func btnClose(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)

    }
    
    @IBAction func btnRegister(_ sender: UIButton) {
    
        let name     = tfName    .text ?? ""
        let userName = tfUserName.text ?? ""
        let pass     = tfPassword.text ?? ""
        let phone    = tfPhone   .text ?? ""
        
        if name.isEmpty {
            myToast(v: self.view, msg: "Please enter name")
            return
        }
        if userName.isEmpty {
            myToast(v: self.view, msg: "Please enter user name")
            return
        }
        if pass.isEmpty {
            myToast(v: self.view, msg: "Please enter password")
            return
        }
        if phone.isEmpty {
            myToast(v: self.view, msg: "Please enter phone")
            return
        }
        
        addNewUser(name: name, userName: userName, pass: pass, phone: phone)
        
    } // btnRegister
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tfName    .delegate = self
        tfUserName.delegate = self
        tfPassword.delegate = self
        tfPhone   .delegate = self

        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func addNewUser(name: String, userName: String, pass: String, phone: String) {
        
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: "MyUser", in: managedContext) else { return }
        let user = MyUser(entity: entity, insertInto: managedContext)
        user.name     = name
        user.userName = userName
        user.password = pass
        user.phone    = phone
        
        do {
            try managedContext.save()
            myToast(v: self.view, msg: "User Saved!")
        } catch let err {
            print("Failded saving \(err)")
            myToast(v: self.view, msg: "Failed")
        }
        
    } // addNewUser
    
    

}
