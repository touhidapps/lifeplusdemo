//
//  ToastLayout.swift
//  LifePlusDemo
//
//  Created by Touhid on 19/3/21.
//

import UIKit

@IBDesignable
class ToastLayout: UIView {
    
    @IBOutlet var title: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func commonInit() {
        layer.masksToBounds = true
        layer.cornerRadius = 8
        layer.borderWidth = 2
        layer.borderColor = #colorLiteral(red: 0, green: 0.6039519906, blue: 0, alpha: 1)
    }

}

import Toast_Swift
public func myToast(v: UIView, msg: String) {
    
    let mView = UINib(nibName: "ToastLayout", bundle: nil).instantiate(withOwner: nil, options: nil).first as! ToastLayout
    
    mView.title.text = msg
    
    mView.frame.size.width = v.bounds.width - 100

    v.showToast(mView, duration: 2.5, position: .center)

}

