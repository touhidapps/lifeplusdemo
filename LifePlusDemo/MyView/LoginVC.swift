//
//  ViewController.swift
//  DemoProject
//
//  Created by Touhid on 3/17/21.
//  Copyright © 2021 Touhid. All rights reserved.
//

import UIKit
import CoreData

class LoginVC: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet var tfName    : UITextField!
    @IBOutlet var tfPassword: UITextField!
    
    
    @IBAction func btnLogin(_ sender: UIButton) {
        
        let name     = tfName    .text ?? ""
        let pass     = tfPassword.text ?? ""
        
        if name.isEmpty {
            myToast(v: self.view, msg: "Please enter user name")
            return
        }
        
        if pass.isEmpty {
            myToast(v: self.view, msg: "Please enter password")
            return
        }
        
        if isUserExists(uName: name, pass: pass) {
            
            MySingleton.shared.userName = name
            
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
            self.transitionVc(vc: vc, type: .fromLeft)
            
            tfName    .text?.removeAll()
            tfPassword.text?.removeAll()
            
        } else {
            myToast(v: self.view, msg: "Invalid User name or password")
        }
        
    } // btnLogin
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tfName    .delegate = self
        tfPassword.delegate = self
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        // to debug
        let d = fetchData()
        d.forEach { (usr) in
            print("User \(usr.name ?? "")--\(usr.userName ?? "")--\(usr.password ?? "")--\(usr.phone ?? "")")
        }
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func isUserExists( uName: String, pass: String ) -> Bool {
        
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let request: NSFetchRequest<MyUser> = MyUser.fetchRequest()
        request.predicate = NSPredicate(format: "userName LIKE %@ AND password LIKE %@", uName, pass)
        
        var results: [NSManagedObject] = []
        
        do {
            results = try managedContext.fetch(request)
        } catch let err {
            print("error executing fetch request: \(err)")
        }
        
        return results.count > 0
        
    } // isUserExists

//    // To get all data
    func fetchData() -> [MyUser]{

        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

        do {
            let users = try managedContext.fetch(MyUser.fetchRequest())
            return users as! [MyUser]
        } catch let error as NSError{
            print("Could not fetch \(error)")
            return []
        }

    }
    
//    func deleteData(_ myUser: MyUser) {
//        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
//        managedContext.delete(myUser)
//        self.save()
//    }

    
    


}

