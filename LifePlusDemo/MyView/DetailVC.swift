//
//  DetailVC.swift
//  DemoProject
//
//  Created by Touhid on 3/17/21.
//  Copyright © 2021 Touhid. All rights reserved.
//

import UIKit

class DetailVC: UIViewController {
    
    var showModel : ShowModel? = nil
    
    @IBOutlet var ivThumb : UIImageView!
    
    @IBOutlet var labelName        : UILabel!
    @IBOutlet var labelGenre       : UILabel!
    @IBOutlet var labelDescription : UILabel!
    
    
    @IBAction func btnBack(_ sender: UIButton) {
        
        transitionVcDismiss(vc: self, type: .fromRight) {
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ivThumb.layer.cornerRadius  = 20
        ivThumb.layer.masksToBounds = true
        ivThumb.layer.borderWidth = 1
        ivThumb.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
        if let mData = showModel?.show?.image?.medium {
            ivThumb.loadFromUrl(path: mData)
        }
        
        if let mData = showModel?.show?.name {
            labelName.text = mData
        }
        
        if let mData = showModel?.show?.genres {
            var g = ""
            mData.forEach { (s) in
                g.append("\(s), ")
            }
            labelGenre.text = String(g.dropLast(2))
        }
        
        if let mData = showModel?.show?.summary?.html2String {
            labelDescription.text = mData
        }
        
    }
    
}
