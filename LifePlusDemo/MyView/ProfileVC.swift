//
//  ProfileVC.swift
//  DemoProject
//
//  Created by Touhid on 3/17/21.
//  Copyright © 2021 Touhid. All rights reserved.
//

import UIKit
import CoreData

class ProfileVC: UIViewController {
    
    
    @IBOutlet var labelName    : UILabel!
    @IBOutlet var labelUserName: UILabel!
    @IBOutlet var labelPhone   : UILabel!
    
    @IBAction func btnBack(_ sender: UIButton) {
        transitionVcDismiss(vc: self, type: .fromLeft, completion: {
            
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let usr = fetchMyData(userName: MySingleton.shared.userName)
        
        labelName    .text = usr?.name     ?? ""
        labelUserName.text = usr?.userName ?? ""
        labelPhone   .text = usr?.phone    ?? ""
        
    }
    
    func fetchMyData( userName: String ) -> MyUser? {
        
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let request: NSFetchRequest<MyUser> = MyUser.fetchRequest()
        request.predicate = NSPredicate(format: "userName LIKE %@", userName)
        
        do {
            let results = try managedContext.fetch(request).first
            return results!
        } catch let error as NSError{
            print("Could not fetch \(error)")
            return nil
        }

    }


}
