//
//  DashboardVC.swift
//  DemoProject
//
//  Created by Touhid on 3/17/21.
//  Copyright © 2021 Touhid. All rights reserved.
//

import UIKit

class DashboardVC: UIViewController {
    
    
    @IBOutlet var indicator: UIActivityIndicatorView!
    
    @IBOutlet var tfSearch: UITextField!
    
    @IBOutlet var collectionView: UICollectionView!
    
    @IBAction func btnLogout(_ sender: UIButton) {
        
        transitionVcDismiss(vc: self, type: .fromLeft) {
            
        }
        
    } // btnLogout
    
    
    @IBAction func btnSearch(_ sender: UIButton) {
        
        let searchText = tfSearch.text ?? ""
        
        if searchText.isEmpty {
            myToast(v: self.view, msg: "Please enter search query")
            return
        }
        
        dashboardViewModel.showSearch(indicator: indicator, searchText: searchText)
        
        tfSearch.resignFirstResponder()
        
    } // btnSearch
    
    
    private let dashboardViewModel = DashboardViewModel()
    
    private var showModelList : [ShowModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dashboardViewModel.dashboardViewModelDelegate = self
        collectionView.delegate = self
        collectionView.dataSource = self
        
        indicator.stopAnimating()
        
    } // viewDidLoad
    
    
    
} // UIViewController


extension DashboardVC: DashboardViewModelDelegate {
    
    func showSearch(showModel: [ShowModel]) {
        
        showModelList = showModel
        collectionView.reloadData()
        
    } // showSearch
    
} // DashboardViewModelDelegate


extension DashboardVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width - 20, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.showModelList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShowCell", for: indexPath as IndexPath) as! ShowCell
        
        cell.backgroundColor     = #colorLiteral(red: 0.9038013816, green: 0.9039530158, blue: 0.9037813544, alpha: 1)
        cell.layer.cornerRadius  = 20
        cell.layer.masksToBounds = true
        cell.layer.borderWidth   = 1
        cell.layer.borderColor   = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        
        cell.imageViewThumb.layer.cornerRadius  = 20
        cell.imageViewThumb.layer.masksToBounds = true
        
        let sml = self.showModelList[indexPath.row]
        
        if let mData = sml.show?.name {
            cell.labelTitle.text = mData
        }
        
        if let mData = sml.show?.summary?.html2String {
            cell.labelDescription.text = mData
        }
        
        if let mData = sml.show?.image?.medium {
            cell.imageViewThumb.loadFromUrl(path: mData)
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
        vc.showModel = self.showModelList[indexPath.row]
        self.transitionVc(vc: vc, type: .fromLeft)
        
    }
    
    
    
} // UICollectionView


