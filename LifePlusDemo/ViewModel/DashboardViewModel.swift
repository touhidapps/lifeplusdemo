//
//  DashboardViewModel.swift
//  LifePlusDemo
//
//  Created by Touhid on 19/3/21.
//

import Foundation
import UIKit

protocol DashboardViewModelDelegate : AnyObject {
    
    func showSearch(showModel: [ShowModel])
    
}


class DashboardViewModel {
    
    let dashboardRepository = DashboardRepository()
    var dashboardViewModelDelegate: DashboardViewModelDelegate?
    
    public func showSearch(indicator: UIActivityIndicatorView?, searchText: String) {
        
        dashboardRepository.showSearch(indicator: indicator, searchText: searchText) { (showModel) in
            
            self.dashboardViewModelDelegate?.showSearch(showModel: showModel)
            
        }
    
    } // showSearch

}
