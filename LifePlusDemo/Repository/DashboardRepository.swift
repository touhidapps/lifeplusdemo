//
//  DashboardRepository.swift
//  LifePlusDemo
//
//  Created by Touhid on 19/3/21.
//

import Foundation
import UIKit

class DashboardRepository {
    

    public func showSearch(indicator: UIActivityIndicatorView?, searchText: String, completion: @escaping ([ShowModel]) -> Void) {
        
        indicator?.startAnimating()
        
        let urlReq = getUrlRequest(mUrl: AllApi.SEARCH_SHOWS + searchText, mMethod: ReqMethods.GET)
        URLSession.shared.dataTask(with: urlReq) { (data, response, error) in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async {
                do {
                    let responseModel = try JSONDecoder().decode([ShowModel].self, from: data)
                    indicator?.stopAnimating()
                    completion(responseModel)
                } catch let err {
                    print(err)
                    indicator?.stopAnimating()
                }
            }
        }.resume()
        
    } // showSearch

}
