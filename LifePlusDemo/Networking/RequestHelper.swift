//
//  RequestHelper.swift
//  LifePlusDemo
//
//  Created by Touhid on 19/3/21.
//

import Foundation

struct ReqMethods {
    static let GET     = "GET"
    static let POST    = "POST"
    static let PUT     = "PUT"
    static let DELETE  = "DELETE"
    static let PATCH   = "PATCH"
}

/**
 Public method without class to access from anywhere
 */

public func getUrlRequest(mUrl: String, mMethod: String) -> URLRequest {
    
    let url = URL(string: mUrl.replacingOccurrences(of: " ", with: "%20"))
    
    var urlReq = URLRequest.init(url: url!)
    urlReq.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
    
    urlReq.httpMethod = mMethod
    urlReq.timeoutInterval = 120 // 120 sec
    return urlReq
    
} // getUrlRequest


public enum AuthType {
    
   case NO_AUTH
   case TOKEN
   case BEARER
    
}

