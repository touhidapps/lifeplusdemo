import Foundation
struct _links : Codable {
    
	let _self : _Self?
	let previousepisode : Previousepisode?

	enum CodingKeys: String, CodingKey {

		case _self = "self"
		case previousepisode = "previousepisode"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		_self = try values.decodeIfPresent(_Self.self, forKey: ._self)
		previousepisode = try values.decodeIfPresent(Previousepisode.self, forKey: .previousepisode)
	}

}
