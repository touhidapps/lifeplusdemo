import Foundation

struct ShowModel : Codable {
	let score : Double?
	let show : Show?

	enum CodingKeys: String, CodingKey {

		case score = "score"
		case show = "show"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		score = try values.decodeIfPresent(Double.self, forKey: .score)
		show = try values.decodeIfPresent(Show.self, forKey: .show)
	}

}
