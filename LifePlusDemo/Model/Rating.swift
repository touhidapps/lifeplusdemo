import Foundation
struct Rating : Codable {
	let average : Double?

	enum CodingKeys: String, CodingKey {

		case average = "average"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		average = try values.decodeIfPresent(Double.self, forKey: .average)
	}

}
